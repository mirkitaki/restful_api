<?php 

    class Router
    {
        private $prefix = '';
        private $routes = [];

        public function add($routeName, Route $route)
        {
            $this->routes[$routeName] = $route; 
        }

        public function match($uri)
        {
            $matchedRoute = null;

            foreach ($this->routes as $route) {
                if ($route->match($uri)) {
                    $matchedRoute = $route;
                    break;
                }
            }

            if ($matchedRoute != '') {
                return $matchedRoute;
            } else {
                return header("HTTP/1.0 404 Not Found");
            }

        }
    }

?>