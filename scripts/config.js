require.config({
    shim: {
        handlebars: {
            exports: 'Handlebars'
        }
    },
    paths: {
        handlebars:     'lib/handlebars', 
        lodash:         'lib/lodash',
        jquery:         'lib/jquery'
    },
    deps: [
        'app'
    ]
});
