define(['Router', 'lodash'], function () {
    "use strict";

    /* constructor */
    var Router = function() {
        window.addEventListener("hashchange", this.execute, false);
    };

    /* vars */
    var routes = {};
    var loadedModules = {};

    /* methods */
    Router.prototype.init = function (callback) { callback.call(); };

    Router.prototype.addRoute = function(pattern, component, callback) {
        pattern = pattern.replace(new RegExp('\\/', "g"), '\/') + '$';
        var route = pattern.split('/')[1].replace('$','');
        routes[route] = {
            callback: callback,
            component: component,
            pattern: pattern
        };

    };

    Router.prototype.execute = function() {
        var activeRoute = window.location.hash.split('/')[1] || 'activity';
        var route = routes[activeRoute];

        var args = [];
        var matches = new RegExp(routes[activeRoute].pattern,'g').exec(window.location.hash);
        if(matches) {
            for (var i = 1, len = matches.length; i < len; i++) {
                args.push(matches[i].replace(/^\s+|\s+$/g, ''));
            }
        }

        if(!(activeRoute in loadedModules)) {
            require(route.component, function() {
                loadedModules[activeRoute] = _.object(route.component, arguments);
                return route.callback.bind(loadedModules[activeRoute]).apply(this, args);
            });
        } else {
            return route.callback.bind(loadedModules[activeRoute]).apply(this, args);
        }
    };


    return new Router();
});


