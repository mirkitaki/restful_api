define('Jobs', ['jquery'], function($) {
	'use strict';

	var Jobs = function() {};

    Jobs.prototype.render = function() {
    // it's global variable, because it's used in Holidays.js
	    var data = JSON.parse($.ajax({url:'/jobs',dataType:"json",async:false}).responseText);

	    var template = {
	    	element : 'body',
	        template: 'templates/Jobs.hbs',
	        data   : {
	        	data: data,
	        	kur: "ehoooo"
	        }
	    };
	    Templates.load(template);
    };

    return new Jobs();
});