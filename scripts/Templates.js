define('Templates',['handlebars','jquery'], function(Handlebars,$) {
    'use strict';

    /* constructor */
    var Templates = function () {};

    /* vars */
    var objPartials = {};

    /* methods */
    Templates.prototype.load = function(obj) {
        obj.custom = obj.custom || '';
        var source = $.ajax({url:obj.template,async:false});
        var compiled = Handlebars.compile(obj.custom+source.responseText);

        if(obj.partial) {
            if(typeof obj.partial !== 'object') {
                var title = obj.partial.split('/')[2].split('.')[0];
                objPartials[title] = $.ajax({url:obj.partial,async:false}).responseText;
                Handlebars.registerPartial(title, Handlebars.compile(objPartials[title]));
            } else {
                for (var prop in obj.partial) {
                    objPartials[prop] = $.ajax({url:obj.partial[prop],async:false}).responseText;
                }
                Handlebars.registerPartial(objPartials);
            }
        }
        $(obj.element).html(compiled(obj.data));
    };

    Templates.prototype.reloadPartial = function(obj) {
        var compiled = [];
        if(obj.length>1) {
            for(var prop in obj) {
                compiled[obj[prop].name] = Handlebars.compile(objPartials[obj[prop].name]);
                obj[prop].callback(compiled[obj[prop].name]);
            }
        }else {
            compiled[obj.name] = Handlebars.compile(objPartials[obj.name]);
            obj.callback(compiled[obj.name]);
        }
    };

    window.Templates = new Templates();

});
