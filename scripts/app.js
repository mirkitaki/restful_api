require(['Router', 'Templates'], function (Router) {
    'use strict';

    Router.init(function() {
    //      Executed wihtou expect route 
    //     Profile.render();
    });

    /*--------------------------------------------------------------------------------------------------*/
    Router.addRoute("/jobs", ['Jobs'], function() {
        this.Jobs.render();
    });
    /*--------------------------------------------------------------------------------------------------*/

    Router.execute();
});
