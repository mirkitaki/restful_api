<?php 

    class Route
    {
        private $params = array();

        public function __construct($route, $options)
        {
            $this->route = $route;
            $this->options = $options;
        }

        public function match($uri)
        {
            $result = preg_match($this->getPattern(), $uri);

            if ($result) {
                $this->parseParams($uri);
            }

            return $result;
        }

        public function getPattern()
        {
            return '#'.preg_replace('#\{\w+\}#', '([0-9a-z]+)', $this->route).'#';
        }

        public function parseParams($uri)
        {
            $paramNames = array();
            $uriParams = array();

            preg_match_all('#\{(\w+)\}#', $this->route, $paramNames);
            preg_match_all($this->getPattern(), $uri, $uriParams);

            for ($i = 0; $i < count($paramNames[1]); $i++) {
                $this->params[$paramNames[1][$i]] = $uriParams[$i + 1][0];
            }
        }

        public function execute()
        {
            if (file_exists(__DIR__.'/controllers/'.$this->options['controller'].'.php')) {
                require_once __DIR__.'/controllers/'.$this->options['controller'].'.php';

                $controllerName = $this->options['controller'];
                $actionName = $this->options["action"];

                $controller = new $controllerName($this->params);
                return $controller->$actionName();
            } else {
                return header("HTTP/1.0 404 Not Found");
            }

        }

    }
