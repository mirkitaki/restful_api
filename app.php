<?php 
    include 'route.php';
    include 'router.php';
    include __DIR__.'/controllers/baseController.php';

    $uri = $_SERVER['REQUEST_URI'];

    $router = new Router();

    $router->add('listJobs', new Route('/jobs', [ 'controller' => 'jobs', 'action' => 'listJobs']));
    $router->add('jobReview', new Route('/jobs/{id}', [ 'controller' => 'jobs', 'action' => 'show']));
    $router->add('listCandidates', new Route('/candidates/list', [ 'controller' => 'candidates', 'action' => 'listCandidates']));
    $router->add('candidateReviews', new Route('/candidates/review/{id}', [ 'controller' => 'candidates', 'action' => 'review']));
    $router->add('searchReviews', new Route('/candidates/search/{id}', [ 'controller' => 'candidates', 'action' => 'search']));

    $currentRoute = $router->match($uri);

    if (!empty($currentRoute)) {
	    echo $currentRoute->execute();
    } else {
    	return header("HTTP/1.0 404 Not Found");
    }
?>