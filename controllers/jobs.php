<?php 

    class Jobs extends BaseController
    {
        function listJobs()
        {
            $stmt = $this->db->query("SELECT * FROM jobs");
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $this->parseToJson($results);
        }

        public function show(){
            $stmt = $this->db->prepare("SELECT * FROM jobs WHERE id = :id");
            $stmt->bindParam(':id', $this->params['id'], PDO::PARAM_INT);
            $stmt->execute();

            $results = $stmt->fetch(PDO::FETCH_ASSOC);
                
            if ($results != false) {
                return $this->parseToJson($results);
            } else {
                return header("HTTP/1.0 404 Not Found");
            }
        }
    }

?>