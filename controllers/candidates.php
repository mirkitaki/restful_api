<?php 

    class Candidates extends BaseController
    {
        
        public function listCandidates(){
            $stmt = $this->db->query("SELECT * FROM candidates");
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $this->parseToJson($results);
        }

        public function review()
        {
            $stmt = $this->db->prepare("SELECT * FROM candidates WHERE id = :id");

            $stmt->bindParam(':id', $this->params['id'], PDO::PARAM_INT);
            $stmt->execute();

            $results = $stmt->fetch(PDO::FETCH_ASSOC);
                
            if ($results != false) {
                return $this->parseToJson($results);
            } else {
                return header("HTTP/1.0 404 Not Found");
            }
        }

        public function search()
        {
            return $this->review();    
        }
    }

?>