<?php 

class BaseController
{
    protected $params;

    public function __construct($params)
    {
        //change userName and Password
        $this->db = new PDO('mysql:host=127.0.0.1;dbname=test;charset=utf8', 'root', '');
        $this->params = $params;
    }

    protected function parseToJson($results)
    {
        return json_encode($results);
    }
}

?>